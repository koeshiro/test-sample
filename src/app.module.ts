import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { RoomsModule } from './rooms/rooms.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';

@Module({
  imports: [ConfigModule.forRoot(), RoomsModule, TypeOrmModule.forRootAsync({
    imports: [ConfigModule],
    async useFactory(configService: ConfigService){
      return {
        type: 'postgres',
        url: configService.get<string>('DB_URL'),
        entities: [join(__dirname, '**', '*.entity.{ts,js}')],
        autoLoadEntities: true,
      }
    },
    inject: [ConfigService],
  })],
  controllers: [],
  providers: [],
})
export class AppModule {}
