import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, OneToMany } from "typeorm";
import { Room } from "./room.entity";

@Entity({
  name: "Bookings"
})
export class Booking {
  @PrimaryGeneratedColumn()
  id: number;
  @ManyToOne(type => Room, room => room.id,{eager:true})
  room: Room;
  @Column()
  from: Date;
  @Column()
  to: Date;
  @Column({nullable:false})
  createdAt: Date;
  @Column({nullable:false})
  updatedAt: Date;
}