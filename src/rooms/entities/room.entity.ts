import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({
  name: "Rooms"
})
export class Room {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  number: string;
  @Column({nullable:false})
  createdAt: Date;
  @Column({nullable:false})
  updatedAt: Date;
}