import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BookingRepository } from './repositories/booking.repository';
import { RoomRepository } from './repositories/room.repository';
import { RoomsController } from './rooms.controller';
import { RoomsService } from './rooms.service';

@Module({
  imports: [TypeOrmModule.forFeature([BookingRepository,RoomRepository])],
  controllers: [RoomsController],
  providers: [RoomsService]
})
export class RoomsModule {}
