import { Test, TestingModule } from '@nestjs/testing';
import { RoomsController } from './rooms.controller';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoomsService } from './rooms.service';
import { BookingRepository } from './repositories/booking.repository';
import { RoomRepository } from './repositories/room.repository';
import { join } from "path"

describe('RoomsController', () => {
  let controller: RoomsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot(),
        TypeOrmModule.forRootAsync({
          imports: [ConfigModule],
          async useFactory(configService: ConfigService) {
            return {
              type: 'postgres',
              url: configService.get<string>('DB_URL'),
              entities: [join(__dirname, '**', '*.entity.{ts,js}')],
              autoLoadEntities: true,
            }
          },
          inject: [ConfigService],
        }),
        TypeOrmModule.forFeature([BookingRepository, RoomRepository])
      ],
      providers: [RoomsService],
      controllers: [RoomsController],
    }).compile();

    controller = module.get<RoomsController>(RoomsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
