import { BadRequestException, Body, Controller, Get, Post, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiOkResponse, ApiBadRequestResponse, ApiTags } from '@nestjs/swagger';
import { Booking } from './entities/booking.entity';
import { Room } from './entities/room.entity';
import { BookingResponseDto, BookRoomRequestDto, FreeRoomsRequestDto, FreeRoomsResponseDto } from './rooms.dtos';
import { RoomsService } from './rooms.service';

@Controller('rooms')
@ApiTags("rooms")
export class RoomsController {
  constructor(protected service: RoomsService) { }
  @Get()
  @UsePipes(new ValidationPipe({
    transform: true,
    transformOptions: { enableImplicitConversion: true },
  }))
  @ApiOkResponse({ type: [FreeRoomsResponseDto], description: "Getting free rooms list" })
  @ApiBadRequestResponse({description: "Error response with out message"})
  async get(@Query() query: FreeRoomsRequestDto): Promise<FreeRoomsResponseDto[]> {
    try {
      return this.service.getFreeRoomsBetween(new Date(query.from), new Date(query.to));
    } catch (e) {
      throw new BadRequestException();
    }
  }
  @Post()
  @ApiOkResponse({ type: BookingResponseDto, description: "Add new booking" })
  @ApiBadRequestResponse({description: "Error response with out message"})
  async post(@Body() data: BookRoomRequestDto) {
    try {
      return this.service.bookRoom(data.roomId, new Date(data.from), new Date(data.to));
    } catch (e) {
      throw new BadRequestException();
    }
  }
}
