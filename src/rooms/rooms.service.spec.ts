import { ConfigModule, ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { BookingRepository } from './repositories/booking.repository';
import { RoomRepository } from './repositories/room.repository';
import { RoomsService } from './rooms.service';

describe('RoomsService', () => {
  let service: RoomsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot(),
        TypeOrmModule.forRootAsync({
          imports: [ConfigModule],
          async useFactory(configService: ConfigService) {
            return {
              type: 'postgres',
              url: configService.get<string>('DB_URL'),
              entities: [join(__dirname, '**', '*.entity.{ts,js}')],
              autoLoadEntities: true,
            }
          },
          inject: [ConfigService],
        }),
        TypeOrmModule.forFeature([BookingRepository, RoomRepository])
      ],
      providers: [RoomsService],
    }).compile();

    service = module.get<RoomsService>(RoomsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('getFreeRoomsBetween', async ()=>{
    const nextDate = new Date();
    nextDate.setDate(nextDate.getDate()+1)
    const results = await service.getFreeRoomsBetween(new Date(),nextDate);
    expect(results.length).toBe(6);
  })

  it('getBookings', async ()=>{
    const nextDate = new Date();
    nextDate.setDate(nextDate.getDate()+1)
    const results = await service.getBookings();
    console.log(results);
    expect(results.length).toBeGreaterThan(0);
  })
});
