import { ApiProperty, ApiResponseProperty } from "@nestjs/swagger";
import { Booking } from "./entities/booking.entity";
import { Room } from "./entities/room.entity";

export class FreeRoomsRequestDto {
  @ApiProperty()
  from: number;
  @ApiProperty()
  to: number;
}

export class BookRoomRequestDto {
  @ApiProperty()
  roomId: number;
  @ApiProperty()
  from: number;
  @ApiProperty()
  to: number;
}

export class FreeRoomsResponseDto {
  @ApiResponseProperty()
  id: number;
  @ApiResponseProperty()
  number: string;
};

export class BookingResponseDto {
  @ApiResponseProperty()
  id: number;
  @ApiResponseProperty({type:FreeRoomsResponseDto})
  room: Room;
  @ApiResponseProperty()
  from: Date;
  @ApiResponseProperty()
  to: Date;
};