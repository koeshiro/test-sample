import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Booking } from './entities/booking.entity';
import { Room } from './entities/room.entity';
import { BookingRepository } from './repositories/booking.repository';
import { RoomRepository } from './repositories/room.repository';

@Injectable()
export class RoomsService {
  constructor(@InjectRepository(Booking) protected bookingRepository: BookingRepository, @InjectRepository(Room) protected roomRepository: RoomRepository) { }

  async getFreeRoomsBetween(from: Date, to: Date) {
    const now = new Date();
    console.log(typeof from, to);
    console.log(from >= now, to > now);
    if ((from >= now && to > now)) {
      throw new Error("Booking dates is not valid");
    }
    from.setHours(0);
    from.setMinutes(0);
    from.setSeconds(0);
    from.setMilliseconds(0);

    to.setHours(23);
    to.setMinutes(59);
    to.setSeconds(59);
    to.setMilliseconds(999);
    return this.roomRepository.getFreeBetweenDates(from, to);
  }

  async bookRoom(roomId: number, from: Date, to: Date) {
    const now = new Date();
    if ((from >= now && to > now)) {
      throw new Error("Booking dates is not valid");
    }
    const book = new Booking();
    book.room = (await this.roomRepository.findOne(roomId));
    book.from = from;
    book.to = to;
    book.createdAt = new Date();
    book.updatedAt = new Date();
    (await this.bookingRepository.insert(book));
    return book;
  }

  async getBookings() {
    return this.bookingRepository.find();
  }
}
