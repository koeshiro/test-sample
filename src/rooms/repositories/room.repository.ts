import { EntityRepository, Repository } from "typeorm";
import { Booking } from "../entities/booking.entity";
import { Room } from "../entities/room.entity";
@EntityRepository(Room)
export class RoomRepository extends Repository<Room> {
  async getFreeBetweenDates(from: Date, to: Date) {
    const subQuery = this.manager.createQueryBuilder(Booking,'b');
    subQuery.select("id").where(`"b"."from" BETWEEN :from AND :to AND "b"."to" BETWEEN :from AND :to`);
    return this.createQueryBuilder('r').where(`ID NOT IN(${subQuery.getQuery()})`,{ from, to }).getMany();
  }
}