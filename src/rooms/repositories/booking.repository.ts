import { EntityRepository, Repository } from "typeorm";
import { Booking } from "../entities/booking.entity";

@EntityRepository(Booking)
export class BookingRepository extends Repository<Booking> {
  async getBetweenDates(from: Date, to: Date):Promise<Booking[]> {
    const query = this.createQueryBuilder("b");
    query.where("(b.from BETWEEN :from AND :to) AND (b.to BETWEEN :from AND :to)", { from, to });
    return query.getMany();
  }
}