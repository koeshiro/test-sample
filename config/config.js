const parseUrl = require("parse-url")
require("dotenv").config();

const url = parseUrl(process.env.DB_URL);
const [username,password] = url.user.length > 0 ? url.user.split(':') : []
module.exports = {
  "development": {
    username,
    password,
    "database": url.pathname.replace('/',''),
    "host": url.resource,
    "dialect": "postgres"
  },
  "test": {
    username,
    password,
    "database": url.pathname.replace('/',''),
    "host": url.resource,
    "dialect": "postgres"
  },
  "production": {
    username,
    password,
    "database": url.pathname.replace('/',''),
    "host": url.resource,
    "dialect": "postgres"
  }
}
