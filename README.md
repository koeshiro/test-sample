# Nest test 

## Docker-compose

Содержит только минимальные настройки для тестового запуска приложения.

```bash
docker-compose up
```

## Migration

```bash
docker exec -it nest_test npm --prefix /usr/src/app run db:migrate
```

## Seed

```bash
docker exec -it nest_test npm --prefix /usr/src/app run db:seed
```

## Test request

```bash
curl http://localhost:3000/rooms/?from=1617829200000&to=1617915599999
```

```bash
curl --location --request POST 'http://localhost:3000/rooms/' \
      --header 'Content-Type: application/json' \
      --data-raw '{"roomId":2,"from":1617829200000,"to":1617915599999}'
```