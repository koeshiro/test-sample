'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const current = new Date();
    current.setDate(current.getDate() + 2)
    const to = new Date();
    to.setDate(current.getDate() + 3);
    return queryInterface.bulkInsert('Bookings', [
      {
        id: 1,
        roomId: 1,
        from: current,
        to,
        createdAt: current,
        updatedAt: current,
      },
    ], {})
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Bookings', null, {})
  }
};
