'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Rooms', [
      {
        id: 1,
        number: '101',
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        id: 2,
        number: '102',
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        id: 3,
        number: '103',
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        id: 4,
        number: '201',
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        id: 5,
        number: '202',
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        id: 6,
        number: '303',
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        id: 7,
        number: '401',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ], {})
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Rooms', null, {});
  }
};
